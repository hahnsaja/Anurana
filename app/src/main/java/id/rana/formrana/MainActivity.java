package id.rana.formrana;

import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final int RESULT_LOAD_IMAGE = 1;
    private EditText edtNama, edtTglLahir, edtAlamat;
    private RadioGroup rdGender;
    private Button btnSimpan;
    private ImageView img;
    private GoogleMap map;

    private int mYear, mMonth, mDay;
    private String gambarPath;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNama = (EditText) findViewById(R.id.edtNama);
        edtAlamat = (EditText) findViewById(R.id.edtAlamat);
        edtTglLahir = (EditText) findViewById(R.id.edtTglLahir);
        rdGender = (RadioGroup) findViewById(R.id.radioSex);
        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        img = (ImageView) findViewById(R.id.imgLoad);

        img.setOnClickListener(this);
        btnSimpan.setOnClickListener(this);
        edtTglLahir.setOnClickListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onClick(View view) {
        if(view == btnSimpan){
            kirimData();
        }
        else if(view == edtTglLahir){
            hiddenKeyboard();
            ambilTglLahir();
        }
        else if(view == img){
            loadImagePermission();
        }
    }

    private void hiddenKeyboard(){
        InputMethodManager inputManager =
                (InputMethodManager) this.
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void ambilTglLahir(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int moy, int dom) {
                datePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
                edtTglLahir.setText(dom + "-" + (moy +1) + "-" + y);
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private void kirimData(){
        if(TextUtils.isEmpty(edtNama.getText().toString())){
            edtNama.setError("Nama harus diisi");
            return;
        }
        if(TextUtils.isEmpty(edtTglLahir.getText().toString())){
            edtTglLahir.setError("Tanggal lahir harus diisi");
            return;
        }
        if(TextUtils.isEmpty(edtAlamat.getText().toString())){
            edtAlamat.setError("Alamat harus diisi");
            return;
        }

        String nama = edtNama.getText().toString();
        String tglLahir = edtTglLahir.getText().toString();
        String alamat = edtAlamat.getText().toString();
        int selectId = rdGender.getCheckedRadioButtonId();
        RadioButton rdButton = (RadioButton) findViewById(selectId);
        String jk = rdButton.getText().toString();

        String[] data = {nama, tglLahir, alamat, jk};

        Intent intent = new Intent(getApplicationContext(), DataActivity.class);
        intent.putExtra("data", data);
        if(imageUri != null){
            intent.putExtra("img", imageUri.toString());
        }
        startActivity(intent);


    }

    //gambar

    private void loadImagePermission(){
        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

        if(permissionCheck != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            loadImageIntent();
        } else {
            loadImageIntent();
        }
    }

    private void loadImageIntent(){
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), RESULT_LOAD_IMAGE);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
            try{

                imageUri = data.getData();

                gambarPath = getPath(this, imageUri);
                final InputStream imageStream = this.getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

//                img.setImageURI(imageUri);
                img.setImageBitmap(selectedImage);
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private String getPath(Context context, Uri uri){

        // DocumentProvider
        if ( DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else
            if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs){
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try{

            if(googleMap != null){
                map = googleMap;
                LatLng bandung = new LatLng(-6.9175, 107.6191);
                map.addMarker(new MarkerOptions().position(bandung).title("Bandungg"));
                map.moveCamera(CameraUpdateFactory.newLatLng(bandung));
                map.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
