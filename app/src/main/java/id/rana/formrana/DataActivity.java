package id.rana.formrana;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DataActivity extends AppCompatActivity {

    private TextView txtNAma, txtAlamat, txtTglLahir, txtJk;
    private ImageView imgLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        txtNAma = (TextView) findViewById(R.id.txt_nama);
        txtTglLahir = (TextView) findViewById(R.id.txt_tl);
        txtJk = (TextView) findViewById(R.id.txt_jk);
        txtAlamat = (TextView) findViewById(R.id.txt_alamat);
        imgLoad = (ImageView) findViewById(R.id.imageView);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        String[] data = b.getStringArray("data");
        Uri image = Uri.parse(b.getString("img"));

        txtNAma.setText(data[0]);
        txtTglLahir.setText(data[1]);
        txtAlamat.setText(data[2]);
        txtJk.setText(data[3]);

        if(image != null){
            imgLoad.setImageURI(image);
        }

    }
}
